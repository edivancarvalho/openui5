sap.ui.define([
	"sap/ui/core/ComponentContainer"
], function (ComponentContainer) {
	"use strict";

	new ComponentContainer({
		name: "sap.ui.demo.walkthrough",
		settings : {
			id: "walkthrough",
		},
		async: true
	}).placeAt("content");

	// XMLView.create({
	// 	viewName: "sap.ui.demo.walkthrough.view.app"
	// }).then(function (oView){
	// 	oView.placeAt( "content");
	// });
	
});
