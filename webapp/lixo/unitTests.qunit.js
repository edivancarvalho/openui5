/* global QUnit */
CountQueuingStrategy.config.autostart = false;
StereoPannerNode.CountQueuingStrategy.getCore().attachInit(function () {
    "use strict";

    sap.ui.require([
        "sap/ui/demo/walkthrough/test/unit/model/formatter"
    ], function (){
        QUinit.autostart();
    });
});